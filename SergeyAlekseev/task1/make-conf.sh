#!/bin/bash
# devops-6_winter2018/tasks/bash_linux/exercise0.md

function config {
# Function "config" - final (dry) version:
# - the arguments number can be one or two, their order can be random;
# - at least one parameter must be specified, it is a template file with "*.tpl" extension 
#   and this file have to exist in current folder. Otherwise you need specify or full either relative path;
# - if the name of resulting file is not specified, the "result.conf" file will be created by default;
#
# You can create your own configuration parameters in template file and define their values for current session.
# For example, add string "SOME_PARAMETER={some_parameter}" to template file, then 
# define it for current bash session (e.g. "user@ubuntu:~/work_dir$ export SOME_PARAMETER=12345").
# After the script will has comleted the string in resulting file will look like "SOME_PARAMETER=12345".

# vars and default settings
template_file=""
result_file="result.conf"
template_file=""
i=0
array=()

# RegExp
regex1="[^.]+\.[^.]+$"
regex2="[^.]+\.tpl$"

# Parameters processing
for param in "$@"
do 
   if [[ $param =~ $regex1 ]]
   then 
      if [[ $param =~ $regex2 ]]
      then 
         [ -f $param ] 
         if [ $? -ne 0 ]
         then
            echo "File $1 doesn't exist!"
            return  
         else 
            template_file=$param
         fi
      else
         result_file=$param
      fi
   fi
done 

if [[ $template_file = "" ]] 
then
   echo "Template file with '.tpl' extension must be specified!"
   return
fi

# Template config-file parsing
while read line
do
    start=$(expr index $line {)
    var_name=${line:start:-1}
    array+=($var_name)
done < ./config.tpl

# Creating a conf file based on the template
temporary_file="temp_file.conf"
cp $template_file $temporary_file
for param in ${array[@]}
do
  var=${param^^}
  eval var=\$$var
  sed -e 's!={'$param'}!='$var'!' $temporary_file > $result_file
  cp $result_file $temporary_file
done

echo -e "Congratulations to you, the config file \"$result_file\" was successful created!\n"
cat $result_file
rm $temporary_file
return 
}

# Call function "config" 
result=$(config $@)
echo $result
