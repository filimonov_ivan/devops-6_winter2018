#!/usr/bin/env bash
#let err=1
cd input
#echo $PWD
#cat f9FrR2NKVC9H2Jzx6B
unset lock_fd
exec {lock_fd}<f9FrR2NKVC9H2Jzx6B || {
echo $?
exit 1
}
echo $lock_fd
flock -n $lock_fd || {
echo $?
exit 2
}
sleep 120
flock -u $lock_fd || {
echo $?
exit 3
}
echo $?