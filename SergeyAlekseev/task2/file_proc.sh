#!/usr/bin/env bash
# devops-6_winter2018/tasks/bash_linux/exercise0.md

function main {
# Function "main" - alfa version:
# the arguments number must be two, if it will be more then only the first two will be used:
# - the first one is relative or absolute path where source files are received
# - and the second one is relative or absolute path where entirely received files will be move.
# The log file named moved_files.log is placed in input dir
# To ensure that you can run multiple instances the script here is used file locking command "flock -n"

# vars and default settings
s_fold=""
d_fold=""
array=()
i=0

# Parse CLI args
for param in "$@"
do
   if ! [[ -d $param ]]
   then
       echo "Path $param doesn't exist!"
       return
   fi
   if [[ $i -eq 0 ]]
        then
            s_fold=$param
   elif [[ $i -eq 1 ]]
        then
            d_fold=$param
   fi
   i+=1
done

cd $s_fold
f_list=$(ls)

# Checking if the file has completely recieved
for file in *
do
    uuid=$(cat "$file" | grep -E "[a-z0-9]{8}-([a-z0-9]{4}-){3}[a-z0-9]{12}")
    if [[  $uuid != "" ]]
    then
        array+=($file)
    else
        continue
    fi
done

# Rename & remove files
for item in ${array[@]}
do
    unset lock_fd
    exec {lock_fd}<$item || continue
#    echo $lock_fd
    flock -n $lock_fd
    if [[ $? -ne 0 ]]
    then continue
    fi
    obj_id=$(cat "$item" | grep -E "[0-9]{4}-[0-9]{2}-[0-9]{2}")
    a=($obj_id)
    obj_name=${a[0]}
    obj_date=${a[1]//-/}
    meta_data=$(stat $item | grep "Modify")
    b=($meta_data)
    file_date=${b[1]//-/}
    new_file_name="$obj_name.$obj_date.$file_date"
    mv $item ../$d_fold/$new_file_name
    echo "File $item was moved from $s_fold to $d_fold as $new_file_name" >> moved_files.log
done
}

# Call main function
result=$(main $@)
echo $result