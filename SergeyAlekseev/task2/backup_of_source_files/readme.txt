This folder contains source files for file-proc.sh script functionality testring.
After each exec of the script it moves some source files from input to output dir.
For each subsequent launch of the script for testing purpose you need to restore
the original state of the files and folders, for which you need to copy all the files
from here to the input folder.
