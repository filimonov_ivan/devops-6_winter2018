#!/usr/bin/env bash
#
#Script for installing Percona MySQL and phpMyAdmin
#

#Ubuntu xenial
function lets_try {
wget https://repo.percona.com/apt/percona-release_0.1-6.$(lsb_release -sc)_all.deb
dpkg -i percona-release_0.1-6.$(lsb_release -sc)_all.deb
apt-get update
apt -yq --allow-unauthenticated -o Dpkg::Options::=--force-confdef \
    install percona-server-server-5.7 apache2 php
apt install phpmyadmin
}

test_dpkg=$(lsof /var/lib/dpkg/lock|wc -l)
while [[ "$test_dpkg" -ne "0" ]]; do sleep 10; done
lets_try
