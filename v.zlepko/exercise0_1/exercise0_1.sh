#!/usr/bin/env bash
#
# First task from https://bitbucket.org/igor_orlangur/devops-6_winter2018
#

function convert_task {
  USAGE="This script is for creating configuration file from template
  Usage:
  $(basename "$0") -h, --help\t\t show this page
   or
  $(basename "$0") template output_file\t convert from template to file
  Start of template should look like:
  \t% { key:value
  \t\"key\":\"value\"
  \t'key':'value'}

  Text for convert should look like:
  \t%(key)s
  "
  # Test paranetrs
  if [ "$1" = "-h" -o "$1" = "--help" ]; then
    echo -e "$USAGE"
    exit 0
  fi
  if [[ -f $1 ]]; then
    TEMPLATE=$1
  else
    echo -e "Error! The template must be set.\n$USAGE"
    exit 1
  fi
  if [ ! -z "$2" ]; then
    if [[ -f "$2" ]]; then
      while true; do
        read -p "File $2 exists. Do you want rewrite file?(Y/N)" yn
        case $yn in
          [Yy]* ) OUTPUT_FILE=$2; break;;
          [Nn]* ) exit;;
          * ) echo "Please answer yes or no.";;
        esac
      done
    else
      OUTPUT_FILE=$2
    fi
  else
    echo -e "Error! The output file must be set.\n$USAGE"
    exit 1
  fi

  #
  # Start convert
  #
  sed -e '/% {/,/}/d' $TEMPLATE > $OUTPUT_FILE
  cat $TEMPLATE|awk '/% {/,/}/'|grep \:|tr -d " "|awk -F\: '{print $1,$2}'|while read KEY VARIABLE
  do
    echo "$VARIABLE"|tr '[:lower:]' '[:upper:]'|while read VARIABLE
    do
      # Delete quotes from keys and variables
      VARIABLE=$(echo "$VARIABLE"|sed -e "s|,$||"|sed -e 's|^["'\'']||g' -e 's|["'\'']$||g')
      KEY=$(echo "$KEY"|sed -e 's|^["'\'']||g' -e 's|["'\'']$||g')
      # Find variables from enviroment
      USER_VARIABLE=$(echo "echo \${${VARIABLE}}"|bash)
      sed -i "s;%(${KEY})s;${USER_VARIABLE};g" $OUTPUT_FILE
    done
  done
  #
  # Remove unused variables
  #
  sed -i -e 's|%(.*)s||g' $OUTPUT_FILE
  }

if [[ "${BASH_SOURCE[0]}" == "${0}" ]]; then
    convert_task "$@"
fi