#!/bin/bash

function file_from_template(){

TemplateName=$1
OutputFileName=$2

# check if two parameters are entered
if [ -z $TemplateName ] || [ -z $OutputFileName ]
then
	echo "Two parameters are expected"
	exit 1
fi

# check if TemplateName enterred correctly and exists
if [ ! -e $TemplateName ]
then
    echo "$TemplateName not found"
	exit 1
fi

# check if TemplateName enterred correctly and exists
# overwrite dialog
if [ -e $OutputFileName ]
then
    echo "$OutputFileName already exists"                                                                                            
	echo " Overwrite ? y/n "
	read ANSWER
	case $ANSWER in
  		[yY]) > $OutputFileName ;;
  		[nN]) exit 1 ;;
		*) exit 1 ;;
	esac
else
	touch $OutputFileName
fi

# replace patterns & write to outputfile
cat $TemplateName | while read line; do
     eval echo `echo $(echo $line | sed -nr 's/\{\%(.*?)\%\}/$\1/p')` >> $OutputFileName
done
}

if [ "${1}" != "--source-only" ]; then
    file_from_template "${@}"
fi